<?php

header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  $messages = array();
  if (!empty($_COOKIE['save'])) {
    setcookie('save', '', 100000);
    $messages[] = 'Спасибо, результаты сохранены.';
  }

  // Складываем признак ошибок в массив.
    $errors = array();
    $errors['name'] = !empty($_COOKIE['name_error']);
    $errors['name1'] = !empty($_COOKIE['name1_error']);
    $errors['e-mail'] = !empty($_COOKIE['e-mail_error']);
    $errors['date'] = !empty($_COOKIE['date_error']);
    $errors['floor'] = !empty($_COOKIE['floor_error']);
    $errors['bio'] = !empty($_COOKIE['bio_error']);

  // Выдаем сообщения об ошибках.
  if ($errors['name']) {
    setcookie('name_error', '', 100000);
    $messages[] = '<div class="error_mes">Заполните имя.</div>';
  }
    if ($errors['name1']) {
    setcookie('name1_error', '', 100000);
    $messages[] = '<div class="error_mes">Перезапишите имя без цифр.</div>';
  }
    if ($errors['e-mail']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('e-mail_error', '', 100000);
    $messages[] = '<div class="error_mes">Заполните email.</div>';
  }
    if ($errors['date']) {
    setcookie('date_error', '', 100000);
    $messages[] = '<div class="error_mes">Заполните дату.</div>';
  }
    if ($errors['floor']) {
    setcookie('floor_error', '', 100000);
    $messages[] = '<div class="error_mes">Заполните пол.</div>';
  }
    if ($errors['bio']) {
    setcookie('bio_error', '', 100000);
    $messages[] = '<div class="error_mes">Заполните биографию.</div>';
  }

  // Складываем предыдущие значения полей в массив, если есть.
  $values = array();
  $values['name'] = empty($_COOKIE['name_value']) ? '' : $_COOKIE['name_value'];
    $values['e-mail'] = empty($_COOKIE['e-mail_value']) ? '' : $_COOKIE['e-mail_value'];
    $values['date'] = empty($_COOKIE['date_value']) ? '' : $_COOKIE['date_value'];
    $values['floor'] = empty($_COOKIE['floor_value']) ? '' : $_COOKIE['floor_value'];
    $values['bio'] = empty($_COOKIE['bio_value']) ? '' : $_COOKIE['bio_value'];

  include('form.php');
}
else {
  // Проверяем ошибки.
  $errors = FALSE;
  $pattern1 = '#[0-9]+#';
  if (empty($_POST['name'])) {
    // Выдаем куку на день с флажком об ошибке в поле name.
    setcookie('name_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
      if( !preg_match($pattern1, $_POST['name'])){
              setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60);
      }
      else{
           setcookie('name1_error', '1', time() + 24 * 60 * 60);
            $errors = TRUE;
      }
  }
    if (empty($_POST['e-mail'])) {
    setcookie('e-mail_error', '2', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('e-mail_value', $_POST['e-mail'], time() + 30 * 24 * 60 * 60);
  }
    if (empty($_POST['date'])) {
    setcookie('date_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('date_value', $_POST['date'], time() + 30 * 24 * 60 * 60);
  }
    if (empty($_POST['floor'])) {
    setcookie('floor_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('floor_value', $_POST['floor'], time() + 30 * 24 * 60 * 60);
  }
    if (empty($_POST['bio'])) {
    setcookie('bio_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('bio_value', $_POST['bio'], time() + 30 * 24 * 60 * 60);
  }

  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
  else {
    // Удаляем Cookies с признаками ошибок.
      
    setcookie('name_error', '', 100000);
      setcookie('name1_error', '', 100000);
    setcookie('e-mail_error', '', 100000);
    setcookie('date_error', '', 100000);
    setcookie('floor_error', '', 100000);
    setcookie('bio_error', '', 100000);
      
    include('index1.php');

  }
  // Сохраняем куку с признаком успешного сохранения.
  setcookie('save', '1');

  // Делаем перенаправление.
  header('Location: index.php');
}
